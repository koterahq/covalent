--[[
	An alternative for Roblox's in-house Localization feature to support reactive states (Fusion.Value).
]]

local Package = script.Parent
local Fusion = require(Package.Fusion)
local PubType = require(Package.PubTypes)
local logError = require(Package.Logging.logError)
local logWarn = require(Package.Logging.logWarn)
local langCodes = require(Package.Linguist.langCodes)

local Value = Fusion.Value
local Computed = Fusion.Computed

local class = {}

local CLASS_METATABLE = {__index = class}

function class:setLang(langCode: PubType.LangCode)
	if table.find(langCodes, langCode) then
		self.lang:set(langCode)
	else
		logWarn("linguistLanguageNotSupported", nil, langCode)
	end
end

function class:get(stringKey: string, ...)
	if self._value[stringKey] then
		local arguments = {...}

		return Computed(function()
			local newArguments = {}
			local formatString = self._value[stringKey][self.lang:get()]
			if not formatString then
				logWarn("linguistTranslationNotFound", self.name, self.lang:get(), self.stringKey)
			end

			for _, argument in arguments do
				if argument.type == "State" and argument.kind == "Value" then
					table.insert(newArguments, argument:get())
				else
					table.insert(newArguments, argument)
				end
			end

			return string.format(formatString or self._value[stringKey]["en"], unpack(newArguments))
		end)
	end

	logError("linguistKeyNotFound", nil, self.name, stringKey)
	return nil
end

local function Linguist(name: string)
	return function(localizationTable)
		local self = setmetatable({
			type = "Linguist",
			name = name,
			lang = Value("en"),
			_value = localizationTable,
		}, CLASS_METATABLE)

		return self
	end
end

return Linguist

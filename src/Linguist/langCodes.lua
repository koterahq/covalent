--[[
  A table of all the language codes available in Roblox's native localization feature.
]]

local Package = script.Parent.Parent
local PubTypes = require(Package.PubTypes)

local langCodes = {
  "sq",
  "ar",
  "bn",
  "nb",
  "bs",
  "bg",
  "my",
  "zh-hans",
  "zh-hant",
  "hr",
  "cs",
  "da",
  "nl",
  "en",
  "et",
  "fil",
  "fi",
  "fr",
  "ka",
  "de",
  "el",
  "hi",
  "hu",
  "id",
  "it",
  "ja",
  "kk",
  "km",
  "lv",
  "lt",
  "ms",
  "pl",
  "pt",
  "ro",
  "ru",
  "sr",
  "si",
  "sk",
  "sl",
  "es",
  "sv",
  "th",
  "tr",
  "uk",
  "vi",
}

return langCodes :: { PubTypes.LangCode }
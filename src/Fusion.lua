local ReplicatedStorage = game:GetService("ReplicatedStorage")

local Package = script.Parent
local logError = require(Package.Logging.logError)

local Fusion = ReplicatedStorage:FindFirstAncestor("Fusion") :: ModuleScript

if not Fusion or not Fusion:IsA("ModuleScript") then
  logError("cannotFindFusion")
else
  return require(Fusion)
end

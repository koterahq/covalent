return {
  cannotFindFusion = "Can't find an ancestor called Fusion in ReplicatedStorage. (have you installed Fusion?)",
  linguistKeyNotFound = "Linguist %s has no key named %s.",
  linguistTranslationNotFound = "Linguist %s has no %s translation for %s, using the source text as replacement for now.",
  linguistLanguageNotSupported = "Language %s is currently not supported in Linguist."
}
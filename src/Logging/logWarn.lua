--!strict

--[[
  Forked from elttob/fusion

	Utility function to log a Covalent-specific warning.
]]

local Package = script.Parent.Parent
local messages = require(Package.Logging.messages)

local function logWarn(messageID, ...)
	local formatString: string

	if messages[messageID] ~= nil then
		formatString = messages[messageID]
	else
		messageID = "unknownMessage"
		formatString = messages[messageID]
	end

	warn(string.format("[Covalent] " .. formatString .. "\n(ID: " .. messageID .. ")", ...))
end

return logWarn
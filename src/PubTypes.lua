--!strict

--[[
  Kinda forked from elttob/fusion

	Stores common public-facing type information for Covalent APIs.
]]

type Set<T> = {[T]: any}

--[[
	General use types
]]

-- A unique symbolic value.
export type Symbol = {
	type: string, -- replace with "Symbol" when Luau supports singleton types
	name: string
}

-- Types that can be expressed as vectors of numbers, and so can be animated.
export type Animatable =
	number |
	CFrame |
	Color3 |
	ColorSequenceKeypoint |
	DateTime |
	NumberRange |
	NumberSequenceKeypoint |
	PhysicalProperties |
	Ray |
	Rect |
	Region3 |
	Region3int16 |
	UDim |
	UDim2 |
	Vector2 |
	Vector2int16 |
	Vector3 |
	Vector3int16

-- Script-readable version information.
export type Version = {
	major: number,
	minor: number,
	isRelease: boolean
}

export type LangCode = 
	"sq" |
  "ar" |
  "bn" |
  "nb" |
  "bs" |
  "bg" |
  "my" |
  "zh-hans" |
  "zh-hant" |
  "hr" |
  "cs" |
  "da" |
  "nl" |
  "en" |
  "et" |
  "fil" |
  "fi" |
  "fr" |
  "ka" |
  "de" |
  "el" |
  "hi" |
  "hu" |
  "id" |
  "it" |
  "ja" |
  "kk" |
  "km" |
  "lv" |
  "lt" |
  "ms" |
  "pl" |
  "pt" |
  "ro" |
  "ru" |
  "sr" |
  "si" |
  "sk" |
  "sl" |
  "es" |
  "sv" |
  "th" |
  "tr" |
  "uk" |
  "vi"

return nil